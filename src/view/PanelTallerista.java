package view;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import componentes.ButtonColumn;
import control.ClienteControl;
import persistence.Tallerista;

import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.awt.event.ActionEvent;

public class PanelTallerista extends JPanel {
	private static final long serialVersionUID = 1L;
	private JTable table;
	private JTextField nombre;
	private JTextField apellido;
	private JTextField email;
	private JTextField edad;
	private DefaultTableModel modelo;
	private ClienteControl cc;
	private int id;
	ImageIcon deleteIcon = new ImageIcon("img/rubbish-bin.png");
	ImageIcon updateIcon = new ImageIcon("img/refresh.png");
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public PanelTallerista() {
		setLayout(null);
		try {
			cc = new ClienteControl();
		} catch (RemoteException e1) {
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			e1.printStackTrace();
		}
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(12, 12, 70, 15);
		add(lblNombre);
		
		nombre = new JTextField();
		nombre.setBounds(12, 31, 114, 19);
		add(nombre);
		nombre.setColumns(10);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(12, 50, 70, 15);
		add(lblApellido);
		
		apellido = new JTextField();
		apellido.setBounds(12, 69, 114, 19);
		add(apellido);
		
		apellido.setColumns(10);
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(12, 88, 70, 15);
		add(lblEmail);
		
		email = new JTextField();
		email.setBounds(12, 107, 114, 19);
		add(email);
		
		email.setColumns(10);
		JLabel lblEdad = new JLabel("Edad");
		lblEdad.setBounds(12, 126, 70, 15);
		add(lblEdad);
		
		edad = new JTextField();
		edad.setBounds(12, 145, 114, 19);
		add(edad);
		edad.setColumns(10);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(12, 170, 117, 25);
		btnGuardar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				//agregar un usuario
				if(cc.addTallerista(nombre.getText(), apellido.getText(), email.getText(),
						Integer.parseInt(edad.getText()))){
					JOptionPane.showMessageDialog(null, "Usuario registrado");
					clean();
					search();
					
				}else {
					System.out.println("Fallo");
					}
				
			}
		});
		add(btnGuardar);
		JButton btnActualizar = new JButton("Actualizar");
		
		btnActualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(cc.updateTallerista(getId(), nombre.getText(),
						apellido.getText(), email.getText(), 
						Integer.parseInt(edad.getText()))) {
					JOptionPane.showMessageDialog(null, "Usuario actualizado");
					clean();
					search();
				}else {
					JOptionPane.showMessageDialog(null, "Usuario no actualizado");
				}
				btnActualizar.setVisible(false);
				btnGuardar.setVisible(true);
			}
		});
		btnActualizar.setBounds(12, 170, 117, 25);
		add(btnActualizar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(138, 12, 590, 406);
		add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		modelo = new DefaultTableModel(
				null,
				new String[] {
					"Id","Nombre", "Apellido", "Email", "Edad", "Eliminar","Actualizar"
				});
		
		table.setModel(modelo);
		table.getColumnModel().getColumn(0).setMaxWidth(0);
		table.getColumnModel().getColumn(0).setMinWidth(0);
		table.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
		table.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);

		Action delete = new AbstractAction()
		{
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e)
		    {
		        JTable table = (JTable)e.getSource();
		        int modelRow = Integer.valueOf( e.getActionCommand() );
		        int id = (int) table.getModel().getValueAt(modelRow, 0);
		        cc.delTallerista(id);
		        ((DefaultTableModel)table.getModel()).removeRow(modelRow);
		    }
		};
		Action update = new AbstractAction()
		{
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e)
		    {

		        JTable table = (JTable)e.getSource();
		        int modelRow = Integer.valueOf( e.getActionCommand() );
		    	System.out.println(modelRow);
		        int id = (int) table.getModel().getValueAt(modelRow, 0);
		        setId(id);
		        Tallerista t = cc.searchTallerista(id);
		        setAll(t.getName(), t.getLast_name(), t.getEmail(), t.getAge());
		        btnGuardar.setVisible(false);
		        btnActualizar.setVisible(true);
		    }
		};
		ButtonColumn buttonColumn = new ButtonColumn(table, delete, 5);
		buttonColumn.setMnemonic(KeyEvent.VK_D);
		ButtonColumn buttonColumn2 = new ButtonColumn(table, update, 6);
		buttonColumn2.setMnemonic(KeyEvent.VK_U);
		
		JButton btnConsultar = new JButton("Consultar");
		btnConsultar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				search();
			}
		});
		btnConsultar.setBounds(12, 207, 114, 25);
		add(btnConsultar);
		

	}
	public void clean() {
		nombre.setText("");
		apellido.setText("");
		email.setText("");
		edad.setText("");
	}
	public void setAll(String nombre, String apellido, 
		String email, int edad) {
		this.nombre.setText(nombre);
		this.apellido.setText(apellido);
		this.email.setText(email);
		this.edad.setText(""+edad);
	}

	public void search() {
		modelo.setRowCount(0);
		for(Tallerista t : cc.searchTallerista()) {
			modelo.addRow(new Object[]{t.getId(),t.getName(),
					t.getLast_name(),t.getEmail(),
					t.getAge(), deleteIcon, updateIcon});
		}
	}
}
