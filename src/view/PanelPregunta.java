package view;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import componentes.ButtonColumn;
import control.PreguntaControl;
import persistence.Pregunta;

import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.awt.event.ActionEvent;

public class PanelPregunta extends JPanel {
	private static final long serialVersionUID = 1L;
	private JTable table;
	private JTextField pregunta;
	private JTextField valor;
	private DefaultTableModel modelo;
	private PreguntaControl pc;
	private int id;
	ImageIcon deleteIcon = new ImageIcon("img/rubbish-bin.png");
	ImageIcon updateIcon = new ImageIcon("img/refresh.png");
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public PanelPregunta() {
		setLayout(null);
		try {
			pc = new PreguntaControl();
		} catch (RemoteException e1) {
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			e1.printStackTrace();
		}
		JLabel lblPregunta = new JLabel("Pregunta");
		lblPregunta.setBounds(12, 12, 100, 15);
		add(lblPregunta);
		
		pregunta = new JTextField();
		pregunta.setBounds(12, 31, 114, 19);
		add(pregunta);
		pregunta.setColumns(10);
		
		JLabel lblValor = new JLabel("Valor");
		lblValor.setBounds(12, 50, 70, 15);
		add(lblValor);
		
		valor = new JTextField();
		valor.setBounds(12, 69, 114, 19);
		add(valor);
		valor.setColumns(10);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(12, 170, 117, 25);
		btnGuardar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				//agregar un usuario
				if(pc.addPregunta(pregunta.getText(),
						Integer.parseInt(valor.getText()))){
					JOptionPane.showMessageDialog(null, "Pregunta registrada");
					clean();
					search();
					
				}else {
					JOptionPane.showMessageDialog(null, "Pregunta no registrada");
					}
				
			}
		});
		add(btnGuardar);
		JButton btnActualizar = new JButton("Actualizar");
		
		btnActualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(pc.updatePregunta(getId(), pregunta.getText(),
						Integer.parseInt(valor.getText()))) {
					JOptionPane.showMessageDialog(null, "Pregunta actualizado");
					clean();
					search();
				}else {
					JOptionPane.showMessageDialog(null, "Pregunta no actualizado");
				}
				btnActualizar.setVisible(false);
				btnGuardar.setVisible(true);
			}
		});
		btnActualizar.setBounds(12, 170, 117, 25);
		add(btnActualizar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(138, 12, 590, 406);
		add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		modelo = new DefaultTableModel(
				null,
				new String[] {
					"Id","Pregunta", "Valor", "Eliminar","Actualizar"
				});
		
		table.setModel(modelo);
		table.getColumnModel().getColumn(0).setMaxWidth(0);
		table.getColumnModel().getColumn(0).setMinWidth(0);
		table.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
		table.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);

		Action delete = new AbstractAction()
		{
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e)
		    {
		        JTable table = (JTable)e.getSource();
		        int modelRow = Integer.valueOf( e.getActionCommand() );
		        int id = (int) table.getModel().getValueAt(modelRow, 0);
		        pc.delPregunta(id);
		        ((DefaultTableModel)table.getModel()).removeRow(modelRow);
		    }
		};
		Action update = new AbstractAction()
		{
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e)
		    {

		        JTable table = (JTable)e.getSource();
		        int modelRow = Integer.valueOf( e.getActionCommand() );
		    	System.out.println(modelRow);
		        int id = (int) table.getModel().getValueAt(modelRow, 0);
		        setId(id);
		        Pregunta t = pc.searchPregunta(id);
		        setAll(t.getPregunta(), t.getValor());
		        btnGuardar.setVisible(false);
		        btnActualizar.setVisible(true);
		    }
		};
		ButtonColumn buttonColumn = new ButtonColumn(table, delete, 3);
		buttonColumn.setMnemonic(KeyEvent.VK_D);
		ButtonColumn buttonColumn2 = new ButtonColumn(table, update, 4);
		buttonColumn2.setMnemonic(KeyEvent.VK_U);
		
		JButton btnConsultar = new JButton("Consultar");
		btnConsultar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				search();
			}
		});
		btnConsultar.setBounds(12, 207, 114, 25);
		add(btnConsultar);
		

	}
	public void clean() {
		pregunta.setText("");
		valor.setText("");
	}
	public void setAll(String pregunta, int valor) {
		this.pregunta.setText(pregunta);
		this.valor.setText(""+valor);
	}

	public void search() {
		modelo.setRowCount(0);
		for(Pregunta t : pc.searchPregunta()) {
			modelo.addRow(new Object[]{t.getId(),t.getPregunta(),
					t.getValor(), deleteIcon, updateIcon});
		}
	}
}
