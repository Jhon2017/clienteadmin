package view;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.table.DefaultTableModel;

import componentes.ButtonColumn;
import componentes.ComboBoxFilterDecorator;
import componentes.CustomComboRenderer;
import control.PreguntaControl;
import control.RespuestaControl;
import persistence.Pregunta;
import persistence.PreguntaRespuesta;
import persistence.Respuesta;

import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;
import java.awt.event.ActionEvent;

public class PanelRespuesta extends JPanel {
	private static final long serialVersionUID = 1L;
	private JTable table;
	private JTextField jtrespuesta;
	private DefaultTableModel modelo;
	private PreguntaControl pc;
	private RespuestaControl rc;
	private Respuesta respuesta;
	List<Pregunta> preguntas;
	JComboBox<Pregunta> comboBox;

	ImageIcon deleteIcon = new ImageIcon("img/rubbish-bin.png");
	ImageIcon updateIcon = new ImageIcon("img/refresh.png");
	public PanelRespuesta() {
		setLayout(null);

		try {
			pc = new PreguntaControl();
			rc = new RespuestaControl();
			respuesta = new Respuesta();
			preguntas = pc.searchPregunta();
		} catch (RemoteException e1) {
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			e1.printStackTrace();
		}
		JLabel lblPregunta = new JLabel("Pregunta");
		lblPregunta.setBounds(12, 12, 100, 15);
		add(lblPregunta);
		//combobox con preguntas
		combox();
		JLabel lblRespuesta = new JLabel("Respuesta");
		lblRespuesta.setBounds(12, 50, 100, 15);
		add(lblRespuesta);
		
		jtrespuesta = new JTextField();
		jtrespuesta.setBounds(12, 69, 114, 19);
		add(jtrespuesta);
		jtrespuesta.setColumns(10);
		JLabel lblCorrecta = new JLabel("Correcta");
		lblCorrecta.setBounds(12, 89, 114, 19);
		add(lblCorrecta);
		JToggleButton toggleButton = new JToggleButton("Da click");
		toggleButton.setBounds(12, 108, 114, 19);
		ItemListener itemListener = new ItemListener() {
		    public void itemStateChanged(ItemEvent itemEvent) {
		        int state = itemEvent.getStateChange();
		        if (state == ItemEvent.SELECTED) {
		            toggleButton.setText("SI");
		            respuesta.setCorrect(1);
		        } else {
		        	toggleButton.setText("NO");
		        	respuesta.setCorrect(0);
		        }
		    }
		};
		toggleButton.addItemListener(itemListener);
		add(toggleButton);
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(12, 170, 117, 25);
		btnGuardar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				//agregar una respuesta
				respuesta.setRespuesta(jtrespuesta.getText());
				if(rc.addRespuesta(respuesta.getId_pregunta(), respuesta.getRespuesta(), respuesta.getCorrect())){
					JOptionPane.showMessageDialog(null, "Respuesta registrada");
					clean();
					search();
					
				}else {
					JOptionPane.showMessageDialog(null, "Pregunta no registrada");
			   }
				
			}
		});
		add(btnGuardar);
		JButton btnActualizar = new JButton("Actualizar");
		
		btnActualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				respuesta.setRespuesta(jtrespuesta.getText());
				if(rc.updateRespuesta(respuesta.getId(), respuesta.getId_pregunta(), respuesta.getRespuesta(), respuesta.getCorrect())) {
			      JOptionPane.showMessageDialog(null, "Pregunta actualizado");
				  clean();
				  search();
				}else {
				    JOptionPane.showMessageDialog(null, "Pregunta no actualizado");
				}
				btnActualizar.setVisible(false);
				btnGuardar.setVisible(true);
			}
		});
		btnActualizar.setBounds(12, 170, 117, 25);
		add(btnActualizar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(138, 12, 590, 406);
		add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		modelo = new DefaultTableModel(
				null,
				new String[] {
					"Id","Respuesta", "Pregunta","Valor","Correcta", "Eliminar","Actualizar"
				});
		
		table.setModel(modelo);
		table.getColumnModel().getColumn(0).setMaxWidth(0);
		table.getColumnModel().getColumn(0).setMinWidth(0);
		table.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
		table.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);

		Action delete = new AbstractAction()
		{
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e)
		    {
		        JTable table = (JTable)e.getSource();
		        int modelRow = Integer.valueOf( e.getActionCommand() );
		        int id = (int) table.getModel().getValueAt(modelRow, 0);
		        rc.delRespuesta(id);
		        ((DefaultTableModel)table.getModel()).removeRow(modelRow);
		    }
		};
		Action update = new AbstractAction()
		{
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e)
		    {

		        JTable table = (JTable)e.getSource();
		        int modelRow = Integer.valueOf( e.getActionCommand() );
		    	System.out.println(modelRow);
		        int id = (int) table.getModel().getValueAt(modelRow, 0);
		        respuesta.setId(id);
		        Respuesta r = rc.searchRespuesta(id);
		        jtrespuesta.setText(r.getRespuesta());
		        btnGuardar.setVisible(false);
		        btnActualizar.setVisible(true);
		    }
		};
		ButtonColumn buttonColumn = new ButtonColumn(table, delete, 5);
		buttonColumn.setMnemonic(KeyEvent.VK_D);
		ButtonColumn buttonColumn2 = new ButtonColumn(table, update, 6);
		buttonColumn2.setMnemonic(KeyEvent.VK_U);
		
		JButton btnConsultar = new JButton("Consultar");
		btnConsultar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				search();
			}
		});
		btnConsultar.setBounds(12, 207, 114, 25);
		add(btnConsultar);
		

	}
	public void clean() {
		jtrespuesta.setText("");
	}

	public void combox() {
		comboBox = new JComboBox<>(
				   preguntas.toArray(new Pregunta[preguntas.size()]));

	      ComboBoxFilterDecorator<Pregunta> decorate = ComboBoxFilterDecorator.decorate(comboBox,
	    		  CustomComboRenderer::getPreguntaDisplayText, 
	    		  PanelRespuesta::preguntaFilter);
	      comboBox.setBounds(12, 31, 114, 19);
	      
          Pregunta p = (Pregunta)comboBox.getSelectedItem();
          respuesta.setId_pregunta(p.getId());
	      ActionListener actionListener = new ActionListener() {
	          public void actionPerformed(ActionEvent actionEvent) {
	              Pregunta p = (Pregunta)comboBox.getSelectedItem();
	              respuesta.setId_pregunta(p.getId());
	          }
	        };
	      comboBox.addActionListener(actionListener);
	      comboBox.setRenderer(new CustomComboRenderer(decorate.getFilterTextSupplier()));
	      add(comboBox);
	}
	
    private static boolean preguntaFilter(Pregunta emp, String textToFilter) {
        if (textToFilter.isEmpty()) {
            return true;
        }
        return CustomComboRenderer.getPreguntaDisplayText(emp).toLowerCase()
                                  .contains(textToFilter.toLowerCase());
    }

	public void search() {
		modelo.setRowCount(0);
		List <PreguntaRespuesta> l = rc.searchRespuesta();
		System.out.println(l);
		for(PreguntaRespuesta r : l) {
			modelo.addRow(new Object[]{r.getId(),r.getRespuesta(),
					r.getPregunta(),r.getValor(),r.getCorrect(), deleteIcon, updateIcon});
		}
	}
}
